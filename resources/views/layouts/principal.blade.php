<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Laravel community Maputo - Covid19</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/product/">

    <!-- Bootstrap core CSS -->
   <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
   <link href="{{asset('css/product.css')}}" rel="stylesheet">
  </head>

  <body>
    @include('layouts.menu.navbar')

    @yield('conteudo')

    @include('layouts.rodape.footer')


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
  </body>
</html>
