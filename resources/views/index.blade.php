@extends('layouts.principal')

@section('conteudo')
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-left jumbotron">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
          <h1 class="display-5 font-weight-normal">COVID-19 Diagnostico </h1>
          <p class="lead font-weight-normal">Saiba sobre o seu estado sobre o COVID-19 e obtenha recomendações sobre os próximos passos a seguir e medidas de prevenção ou propagação do vírus.</p>
          <a class="btn btn-primary btn-lg start" href="#">Iniciar o Diagnostico</a>
    
        </div>
        <div class="product-device box-shadow d-none d-md-block"></div>
        <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
    
      </div>   

      <div class="container py-5">
          <div class="row">
            
            <div class="col-6 col-md">
                <h5>Conteudos</h5>
                <ul class="list-unstyled text-small">
                    <p>It uses utility classes for typography and s
                        pacing to space content out within the larger container.</p>
                    <p class="lead">
                      <a class="btn btn-outline-info" href="#" role="button">Continuar a ler...</a>
              </div>
              <div class="col-6 col-md">
                <h5>Conteudos</h5>
                <ul class="list-unstyled text-small">
                    <p>It uses utility classes for typography and s
                        pacing to space content out within the larger container.</p>
                    <p class="lead">
                        <a class="btn btn-outline-info" href="#" role="button">Continuar a ler...</a>
                    </div>
              <div class="col-6 col-md">
                <h5>Conteudos</h5>
                <ul class="list-unstyled text-small">
                    <p>It uses utility classes for typography and s
                        pacing to space content out within the larger container.</p>
                    <p class="lead">
                        <a class="btn btn-outline-info" href="#" role="button">Continuar a ler...</a>
                    </div>
                    <div class="col-6 col-md">
                        <h5>Conteudos</h5>
                        <ul class="list-unstyled text-small">
                            <p>It uses utility classes for typography and s
                                pacing to space content out within the larger container.</p>
                            <p class="lead">
                                <a class="btn btn-outline-info" href="#" role="button">Continuar a ler...</a>
                            </div>
          </div>

          <hr class="my-4">

          <div class="row">
              <div class="container text-center" style="width: 400px">
                <h4 class="display-5 font-weight-normal text-center">Desenvolvedires e Parceiros </h4>
                <br>
                
                <i class="fa fa-camera-retro fa-5x"></i> 
                <i class="fa fa-camera-retro fa-5x"></i> 
                <i class="fa fa-camera-retro fa-5x"></i>    
                <br><br>

                
                <p >Este aplicativo foi densevolvido pela comunidade de Desenvolvedores laravel Maputo
                     em parceria com X Y Z</p>




              </div>
        </div>


      </div>

      </div>



{{--
    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
    <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
      <div class="my-3 p-3">
        <h2 class="display-5">Another headline</h2>
        <p class="lead">And an even wittier subheading.</p>
      </div>
      <div class="bg-white box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
    </div>
    <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
      <div class="my-3 py-3">
        <h2 class="display-5">Another headline</h2>
        <p class="lead">And an even wittier subheading.</p>
      </div>
      <div class="bg-white box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
    </div>
  </div>
    
    --}}  
@endsection